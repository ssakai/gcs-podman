# What's This?
Globus Connect Server (GCS) has some requirements that makes it disruptive to install unless the host is dedicated solely to GCS. These requirements include an Apache HTTPD, and ability to bind to to 443/tcp and 50000-51000/tcp.  

This repo contains bits and pieces necessary to build and run a containerized GCS using Podman.  

## Wait, not Docker?
Docker brings along an additional level of complexity, including a daemon. Podman doesn't use a daemon and doesn't automate as much to make things "just work".  

# Oops, It's broken.
First things first, you're probably reading this because you want to make use of this, or something is broken. The latter is more likely.

##  No IP addresses available in range set.
You see something like this when trying to start or run a container:

```
[root@plinky net.d]# podman run -it --rm --net=podman-78  alpine sh
ERRO[0000] Error adding network: failed to allocate for range 0: no IP addresses available in range set: 198.202.90.78-198.202.90.78
ERRO[0000] Error while adding pod to CNI network "podman-78": failed to allocate for range 0: no IP addresses available in range set: 198.202.90.78-198.202.90.78
Error: error configuring network namespace for container 8f7b8d920f133f80acbdcccfcaa599c24e6311a21cca7a65424dd389e91c07c0: failed to allocate for range 0: no IP addresses available in range set: 198.202.90.78-198.202.90.78
```  

If a container fails to start, in some cases its podman-ipam allocated IP will not get freed.  Fortunately this is just a file.

```
# remove orphaned locks for ips and try again.
[root@plinky net.d]# rm /var/lib/cni/networks/podman-78/198.202.90.7*
rm: remove regular file \u2018/var/lib/cni/networks/podman-78/198.202.90.78\u2019? y
[root@plinky net.d]# podman run -it --rm --net=podman-78  alpine sh
/ #
```

## Sudo: account validation failure in entrypoint
This happens when there's no `getent shadow root` entry. You'll need a minimal `/etc/shadow` which contains just an entry for root and a disabled password.


## Updated Mounts
Hopefully the mount list in `make-container.sh` was low enough that additional project mounts are just picked up by the containing directory.

If changes are required, edit the `collection-dirs` file adjacent to the `make-container.sh` script, and for each GCS node, `make-container.sh <ip>`, start the container (either with podman or systemctl if a unit exists).

# Usage
## High-Level Requirements
- [ ] Podman system packages (rpm/deb)
- [ ] NSCD on container host, listening on `/var/run/nscd/socket`
- [ ] System accounts for `gcsweb:gcsweb` and `www-data:www-data` - UID/GID do not matter, but must not overlap with any other accounts. The groups must not have any other accounts in them.
- [ ] Networking (One of the following)
  - [ ] `host` - use the container-host's current IP. (Note: This occupies ports 443/tcp, gridftp-related ports)
  - [ ] `macvlan` + dedicated vlan interface carrying a separate IP subnet - use a separate IP network without interfering with container-host services.
  - [ ] `bridge`  + bridge interface assigned the container-host's public IP - use a separate IP on the same public network without interfering with container-host services.
  - [ ] **DO NOT USE** `macvlan` + container-host's public interface - Manager container and container-host will not be able to open a connection to GCS services in container.
  
- [ ] (If not using `host`) Podman network definitions for network names starting with `gcs-` - The CentOS 7 version of podman doesn't support `podman create ... --ip=...`, so the network definition needs to have a dynamic range of a single IP, and each IP needs its own definition, even if they're for containers in the same vlan.
- [ ] Systemd unit file for GCS containers (optional, but required if GCS containers need to come up on reboot).
- [ ] Globus ROLE account, not your personal one.
- [ ] ID mapping solution.

## Additional Prereqs
* Review the installation guides for setting up the GCS Node and data access. When using the `globus-connect-server` CLI utility, you'll be in the `gcs-manager` container.
  * https://docs.globus.org/globus-connect-server/v5/
  * https://docs.globus.org/globus-connect-server/v5/data-access-guide/
* Logging needs to go somewhere, currently in tmpfs. TBD, maybe lustre or NFS?

## Cluster-Specific
### Comet (Expanse) DM
- [x] System accounts for `gcsweb` and `www-data` are in SAM, TRS TD.
- [x] `macvlan` networking on `bond0.1997`.
  - May need to `rocks sync host interface <host>` on `oasis-whale` if missing.
    - Will need to `pcs cluster stop && pcs cluster start` on affected node if interfaces get updated by Rocks. If done quickly, nobody will notice that the HA ip disappeared during the sync.
- [x] Podman network definitions for `gcs-` networks in `oasis-whale` cfengine. Run `/opt/sdscsec/sbin/cfengine-run` to update node.
  - /etc/cni/net.d/gcs-192.67.81.148.conflist
  - /etc/cni/net.d/gcs-192.67.81.149.conflist
  - /etc/cni/net.d/gcs-192.67.81.150.conflist
  - /etc/cni/net.d/gcs-192.67.81.151.conflist
- [x] Systemd unit file in cfengine: `/etc/systemd/system/gcs@.service`
  - It's a template, so just enable systemd unit `gcs@<ip>.service` instead of creating individual service files.
- [x] Globus account: `sdsc@globusid.org`
- [x] Endpoint is already created. Management container is on `comet-dm4-15-4`.
  - [x] Secrets are in `oasis-whale:/var/secrets/gcs` and `oasis-whale:/var/secrets/gcs-manager-persist`. Use `rsync -avH ...` to copy over if missing.
- [x] Host-to-container mapping:
  - `comet-dm1-15-1` -> gcs@192.67.81.148
  - `comet-dm2-15-2` -> gcs@192.67.81.149
  - `comet-dm3-15-3` -> gcs@192.67.81.150
  - `comet-dm4-15-4` -> gcs@192.67.81.151
- [x] `gcs-podman` source is in NFS: `/share/apps/src/gcs-podman`
- [x] ID mapping solution provided by `fetchidmap` executed by cron. 
  
#### Deploying on Comet DM Host
- [ ] Establish `bond0.1997` interface with Rocks if missing.
- [ ] Run cfengine if `/etc/cni/net.d/gcs-*` files missing.
- [ ] Rsync `/var/secrets/gcs*` from `oasis-whale` if missing.
- [ ] Build container image.
- [ ] Make `gcs-<ip>` container.
- [ ] Manually invoke `fetchidmap` to establish mapping.
- [ ] Install `gcs5-stuff` crontab in `/etc/cron.d/`.
- [ ] Instantiate systemd unit for `gcs@<ip>`.
- [ ] Start systemd unit.
- [x] Not much else to do, will join existing endpoint.
  - Examine output of `gcs-manager:/# globus-connect-server node list`

## General Notes
### Create Globus ID and Endpoint If Needed
See Globus Connect installation guide for latest info.
* https://developers.globus.org <-- Create new endpoint. 

### Podman Networks Have an Expected Name Format
The `make-container.sh` script expects to see a podman network named `gcs-<ip>`. It must dole out a single IP (equal to <ip>).

### Create Image On Each Container-Host
Use `build-image.sh` to create an image tagged  `globus-connect-server54:latest`.  Each time this is run, a new image will be created and the above tag moved to it.

### Manager Container Needs Persistent Dirs
The manager container needs persistent storage so you don't have to keep logging in to Globus. Some directories will need to be created in `/var/secrets` with these permissions and ownership (the container entrypoint will exit if the permissions are bad): 
```
drwx--x---   2 root gcsweb 4096 Aug 12 19:22 gcs
drwx------   2 root root   4096 Aug 12 19:24 gcs-manager-persist
```
### Manager Container Required for Each Endpoint (not node!)
Use `make-container.sh manager` to create the manager container.  

### GCS Nodes Need Secrets Too!
* Obtain gcs client id and gcs client secret from  `developers` site.
* Stash client id in `/var/secrets/gcs/gcs-client-id.txt` (one line).
* Stash client secret in `/var/secrets/gcs/gcs-client-secret.txt`.

### Enter `gcs-manager` Container to Manage GCS Endpoint
* Start the `gcs-manager` container.  It will stick around for about 20 minutes before exiting.
  ```
  podman start gcs-manager
  ```
* Enter the `gcs-manager` container.
  ```
  podman exec -it gcs-manager bash
  ```

#### Reminder: / is *READ ONLY*
```
root@gcs-manager:/# globus-connect-server endpoint setup "Test GCS5 endpoint" --organization "SDSC" --client-id 
...
Please read the Terms of Service at:
https://letsencrypt.org/repository/
Do you agree to these Terms of Service? [y/N]: y
OSError: [Errno 30] Read-only file system: '/globus-connect-server.log'
root@gcs-manager:/# cd /root
```
Use CWD `/root`.

### Example: Setup Endpoint
* https://docs.globus.org/globus-connect-server/v5/reference/endpoint/setup/
* The `globus-connect-server endpoint setup...` command DOES take a few minutes at the end, don't worry, it's (probably) not an error.
* This produces a `deployment-key.json` file in CWD (hint: try `cd /tmp` or `cd /dev/shm` or `cd /root`).
* Put a copy in `/secrets/gcs-deployment-key.json`.
  * Change permissions and ownership as follows.
  ```
  -r--------. 1 root root 1615 Aug  8 17:48 /var/secrets/gcs/gcs-deployment-key.json
  ```
* Remove the original.
  
### Example: Log in to Endpoint
* Log in to GCS endpoint: `globus-connect-server login $(cat /secrets/gcs-client-id.txt)`.
* `endpoint id` is the same as the client id.
* This will involve copy-pasting a URL and a response code.
* Use the ROLE account intended for managing this GCS deployment.
* Note: it's normal to not be able to config/update the endpoint at this time.  
```
root@gcs-manager:/# globus-connect-server endpoint show
Error contacting the GCS Manager API
Error resolving 85ff8.0ec8.data.globus.org
This may be because the endpoint is deleted, it is not deployed on any
nodes, or your DNS resolver is misconfigured.
root@gcs-manager:/#
```

### Example: Create Node Config
* Make node configs (for 198.202.90.79): 
```
root@gcs-manager:/# cd /dev/shm
root@gcs-manager:/dev/shm# GLOBUS_CLIENT_SECRET=$(cat /secrets/gcs-client-secret.txt) globus-connect-server node create --export-node /secrets/198.202.90.79.nodecfg.json -c $( cat /secrets/gcs-client-id.txt ) --deployment-key /secrets/gcs-deployment-key.json 
root@gcs-manager:/dev/shm# ls -la /secrets/198.202.90.79.nodecfg.json 
-rw-------. 1 root root 1724 Aug 10 02:39 /secrets/198.202.90.79.nodecfg.json
root@gcs-manager:/dev/shm# chmod 400 /secrets/198.202.90.79.nodecfg.json
```
Note that the exported JSON goes in `/secrets/<ip>.nodecfg.json`.  
Note that the permissions are `400` for the nodecfg.json file. The container entrypoint will exit if the permissions are wrong.
  
### Creating Containers
Run `make-container.sh <ip>` for each IP / GCS node you want to possibly run on this host. Containers do not start by default, so you can safely define all nodes on each container host, however the de-centralized design of GCS doesn't require this.
* Note: It's okay if there's a warning about not being able to evict container.  
* Example for 192.67.81.151:
```
-bash-4.2$ sudo ./make-container.sh 192.67.81.151
Error: Failed to evict container: "": Failed to find container "gcs-192.67.81.151" in state: no container with name or ID gcs-192.67.81.151 found: no such container
717c36083a1624c0920bcf77a40719a2264d96328750ef159edb1e058b0b6dcd
New container gcs-192.67.81.151
Start with:
  podman start gcs-192.67.81.151
-bash-4.2$  
```

### Starting Containers
Run `podman start gcs-<ip>` to start the GCS node.  
If you have deployed the systemd unit template, enable the instance and start it that way.  
* Example for 192.67.81.151:
```
-bash-4.2$ sudo systemctl enable gcs@192.67.81.151
Created symlink from /etc/systemd/system/multi-user.target.wants/gcs@192.67.81.151.service to /etc/systemd/system/gcs@.service.
-bash-4.2$ sudo systemctl start !$
sudo systemctl start gcs@192.67.81.151
-bash-4.2$ 
```
* Note: Once at least one node is running, the endpoint can be managed.
```
-bash-4.2$ sudo podman exec -it gcs-manager bash
root@gcs-manager:/# globus-connect-server endpoint show
Display Name:    SDSC HPC Data Movers
ID:              ba05bd44-4422-48ab-a110-842e0edd107c
Subscription ID: None
Public:          True
GCS Manager URL: https://85ff8.0ec8.data.globus.org
Network Use:     normal
Organization:    San Diego Supercomputer Center
Contact E-mail:  consult@sdsc.edu
root@gcs-manager:/# 
```

### Create Collection, Policies, Etc.
* https://docs.globus.org/globus-connect-server/v5/data-access-guide/
* Notable constraints/deviations:
  * Use `--restrict-paths file:/secrets/restrict-paths.json`, which you will probably have to copy to `/var/secrets/gcs/` to get it in the container.
  * Accordingly, the base_path for the collection is simply `/export` or a directory under it.
  * Use `--identity-mapping external:/mapapp.py` if gridmap-style mapping is employed. (More later.)

#### Example creation: Storage Gateway
```
root@gcs-manager:/# globus-connect-server storage-gateway create posix --domain access-ci.org --domain ucsd.edu --domain xsede.org --domain globusid.org --identity-mapping external:/mapapp.py --restrict-paths file:/default-restrict-paths.json "SDSC HPC Data Movers"
Storage Gateway ID: 50fb860b-3505-4f93-88b8-6f6de4f40177
root@gcs-manager:/# 
```

#### Example creation: Collection
```
globus-connect-server collection create 50fb860b-3505-4f93-88b8-6f6de4f40177 /export/projects 'SDSC HPC - Projects' --force-encryption --description 'Managed storage for projects'
Collection ID: 46cad570-3f0d-4bfc-8ebc-ce2c13f13df7
```

# Architecture
## Networking
Each container runs with its own (public) IP address. This IP address is tied to a GCS node, and generally should use the same unique `.nodecfg.json` as when the node config is first used to setup a node. In other words, don't share a `.nodecfg.json` between IPs, or mix-and-match them.  

### IP Address Management
Newer versions of podman support the `--ip` argument, which specifies the IP address to assign to a container. The version shipped for CentOS 7 does not support this.  

To get around that limitation, we create a podman network for each GCS node, and use Podman's IPAM to assign an IP out of a range of 1.

### Podman Bridge
Podman supports a few modes of operation. We use the 'bridge' mode, with a pre-configured (by the OS and sysadmin) bridge. This is not to be confused with the default bridge mode, which sticks every container behind a NAT gateway.

### Macvlan Alternative
If an IP subnet separate from the container host's IP is available, macvlan networking can be used instead of bridging. The network path from the host to the containers' IP *must* cross a routing boundary, otherwise they can't reach each other. This is an intentional behavior of macvlan.

## Exported Filesystems
GCS *Collections* have a base_path, which determines where the chroot of the collection is served from. These should all be configured to `/export` or something further down. Do not set `/` as the base_path, as there's nothing for users in there.  

Similarly, all user-accessible filesystems that need to be served over GCS should be bind-mounted into `/export`. The `make-container.sh` does this.  

Consider **NOT** serving `/home` with GCS. Doing so will allow bypassing the authentication controls in place for the associated (*Expanse*, *TSCC*, *Comet*, etc.) cluster. Think: *Where does `authorized_keys` and `.google-authenticator` live?*

## Container Innards
Other details about a GCS container.

### Read-Only
The container is read-only except for a subset of the directory tree.

#### Notable Exceptions
* /root (tmpfs on node, bind mount from `/var/secrets/gcs-manager-persist` on manager for CLI state)
* /export/* (bind mounts from container host to serve via GCS)
* /secrets (bind mount from `/var/secrets/gcs`, readonly on node, readwrite on manager)

### No SUID / SGID
The `setuid` and `setgid` bits on all files in the image have been removed.

### Passwd and Group
~~* The `passwd` and `group` files from the container host are bind-mounted in to the container, read-only.~~
* The nscd service is used from the container host. Please make sure it is running. This container will have the same passwd and group entries.
* We're doing this because the container startup process tries to force a write on the `passwd` file, which succeeds by creating a overlaid file instead of failing. The overlay file does not receive updates from ART.

## Identity Mapping
The source of identities comes from the production *Edamame* instance. 
* (edamame base url)/api/v1/renderMappingAsGridmap -> /var/secrets/gcs/idmap.txt
The `mapapp.py` script (in container) will look for `/secrets/idmap.txt`. It runs as `gcsweb` and the scripts that check permissions on `/var/secrets/gcs` are adjusted accordingly. Expecting mode `0710` owned by `root:gcsweb`. This script has been tested with 20,000 entries and appears to execute responsively.
* Changing active user in the middle of the session: Globus GUI will hang for a few seconds, retry operation, and pick up new local user.
  

