#!/bin/bash

###############################################################################
# WARNING: Launching GCS without systemd is experimental. Use at your own risk.
# Compatible with GCS 5.4.21+
###############################################################################

umask 0077
set -e 
set -u

# this dir needs to contain the following:
# gcs-deployment-key.json
# gcs-client-secret.txt
# gcs-client-id.txt
# <ip>.nodecfg.json
# it must only be readable by root.
SECRETS_BASE_DIR=/secrets/

# The gcsweb group and user accounts must exist, because this is what
# gcs expects to use.
getent passwd gcsweb > /dev/null
if [[ $? -ne 0 ]]; then
  echo "The user 'gcsweb' appears to be missing."
  exit 1
fi
getent group gcsweb > /dev/null
if [[ $? -ne 0 ]]; then
  echo "The group 'gcsweb' appears to be missing."
  exit 1
fi


#
# ~~The parameters we need to launch 'node setup' are passed in via environment
# variables.~~
#
# We'll actually pull this from a bind mount because:
# 1) There's no secrets broker.
# 2) The host running this container already has a place to put secrets.
# 3) The cluster management process for this host has a way to put secrets in (2).

# But first, we'll need the container's IP, since each GCS node's identity
# is tied to an IP.
# Docs recommend -I since -i requires DNS resolution.
IPADDRS=( $(hostname -I) )

# We don't have a good way to decide which IP is used on a multihomed
# container, so just don't multihome.
if [[ "${#IPADDRS[@]}" -ne 1 ]]; then
  printf "This container has %d IPs; expecting exactly 1\n" "${#IPADDRS[@]}" 1>&2
  exit 1;
fi

MYIP="${IPADDRS[0]}"

# Check the base secrets directory for weird perms.
if [[ ! -d "${SECRETS_BASE_DIR}" ]]; then
  echo "Missing SECRETS_BASE_DIR: ${SECRETS_BASE_DIR}" 1>&2
  exit 1
fi
if [[ $( stat -c %a "${SECRETS_BASE_DIR}" ) != "710" ]]; then
  echo "Bad permissions on ${SECRETS_BASE_DIR}: expecting 710" 1>&2
  exit 1
fi
if [[ $( stat -c %u "${SECRETS_BASE_DIR}" ) != "0" ]]; then
  echo "Bad ownership on ${SECRETS_BASE_DIR}; expecting 0" 1>&2
  exit 1
fi
if [[ $( stat -c %G "${SECRETS_BASE_DIR}" ) != "gcsweb" ]]; then
  echo "Bad group ownership on ${SECRETS_BASE_DIR}; expecting gcsweb" 1>&2
  exit 1
fi


# Let's make sure all the files are there.
# Similar checks as base dir
for I in gcs-deployment-key.json gcs-client-secret.txt gcs-client-id.txt "${MYIP}.nodecfg.json"; do
  CHECKFILE="${SECRETS_BASE_DIR}/${I}"
  if [[ ! -f "${CHECKFILE}" ]]; then
    echo "Missing file: ${CHECKFILE}" 1>&2
    exit 1
  fi
  STATSTR=$( stat -c %a "${CHECKFILE}" )
  if [[ "${STATSTR}" != "400" ]]; then
    echo "Bad permissions on ${CHECKFILE}: expecting 400 got ${STATSTR}" 1>&2
    exit 1
  fi
  STATSTR=$( stat -c %u "${CHECKFILE}" )
  if [[ "${STATSTR}" != "0" ]]; then
    echo "Bad ownership on ${CHECKFILE}: expecting 0 got ${STATSTR}" 1>&2
    exit 1
  fi
done
  
GLOBUS_CLIENT_ID=$( cat "${SECRETS_BASE_DIR}/gcs-client-id.txt" )
GLOBUS_CLIENT_SECRET=$( cat "${SECRETS_BASE_DIR}/gcs-client-secret.txt" )
DEPLOYMENT_KEY_FILE="${SECRETS_BASE_DIR}/gcs-deployment-key.json"
NODE_CONFIG_FILE="${SECRETS_BASE_DIR}/${MYIP}.nodecfg.json"

if [ -z "$GLOBUS_CLIENT_ID" ]
then
    echo "${SECRETS_BASE_DIR}/gcs-client-id.txt appears to be empty."
    exit 1
fi

if [ -z "$GLOBUS_CLIENT_SECRET" ]
then
    echo "${SECRETS_BASE_DIR}/gcs-client-secret.txt appears to be empty."
    exit 1
fi


# copy some state data to tmpfs
cp -ar /var/lib/globus-connect-server-dist/* /var/lib/globus-connect-server/
chown gcsweb:gcsweb /var/lib/globus-connect-server/gcs-manager
chmod 700 /var/lib/globus-connect-server/gcs-manager
cp -ar /etc/grid-security-dist/* /etc/grid-security/

# we're replacing /etc/passwd and /etc/group, so make sure to fix ownership.
for I in \
  /var/lib/globus-connect-server/gcs-manager \
  /var/lib/globus-connect-server/gcs-manager/etc/domains \
  /var/lib/globus-connect-server/gcs-manager/etc/httpd/reload \
  /var/lib/globus-connect-server/gcs-manager/etc/letsencrypt \
  /var/lib/globus-connect-server/gcs-manager/gcs54.db \
  /var/lib/globus-connect-server/gcs-manager/locks \
  /var/lib/globus-connect-server/info.json \
  /var/log/globus-connect-server/gcs-manager/gcs.log \
  /var/log/globus-connect-server/letsencrypt ; do
  if [[ -e "${I}" ]]; then
    chown gcsweb:gcsweb "${I}"
  fi
done

chown gcsweb:root /var/lib/globus-connect-server/gcs-manager/etc/httpd/conf.d

###############################################################################
# STEP 1: Configure the node for Globus by running 'node setup'
###############################################################################

#
# Normally, 'globus-connect-server node setup' will pull down the node's Globus
# configuration and launch all necessary local services. However, since we are
# working around the use of systemd in order to have an unprivileged container,
# we have to handle these steps ourselves.
#

# GCS will check for the existence of this file to make sure this is
# a supported platform. Make it a fake so that we can handle service
# launch ourselves.
#
# This was handled during image creation as the image is read-only.

# The deployment key is kept in a bind mount, so no need to put it
# in the environment or make another copy.

GLOBUS_CLIENT_SECRET="${GLOBUS_CLIENT_SECRET}" globus-connect-server node setup     \
    --client-id "${GLOBUS_CLIENT_ID}"    \
    -s "${GLOBUS_CLIENT_SECRET}"        \
    --deployment-key "${DEPLOYMENT_KEY_FILE}" \
    --import-node "${NODE_CONFIG_FILE}" 

if [ $? -ne 0 ]
then
    echo "node setup failed. Exiting."
    exit 1
fi

###############################################################################
# STEP 2: Launch and monitor services in lieu of systemd
###############################################################################

###
### Launch GCS Manager
###
echo 'Launching GCS Manager'

#
# This is the API process that allows you to administer the endpoint
# configuration as well as authorizing users that are accessing your endpoint
# using the Transfer service.
#

# systemd executes gcs_manager.socket and creates /run/gcs_manager.sock on our
# behalf. It is used for communication between apache/httpd and the GCS Manager.
# /run/ is not writable by gcsweb though so it can not create the socket itself.
# Therefore, we move the socket (and pid file) to a sub directory and give GCSM
# write access. Then we create a symlink between the old and new locations so
# that apache/httpd can still find the socket.

mkdir /run/gcs_manager
chown gcsweb:gcsweb /run/gcs_manager
ln -s /run/gcs_manager/sock /run/gcs_manager.sock
chmod 755 /run/gcs_manager

# Launch the service. See /lib/systemd/system/gcs_manager.service
(
    cd /opt/globus/share/web;
    sudo -u gcsweb -g gcsweb /opt/globus/bin/gunicorn \
        --workers 4                                   \
	--preload api_app                             \
	--daemon                                      \
	--bind=unix:/run/gcs_manager/sock             \
	--pid /run/gcs_manager/pid
)

# Wait for the pid file which signals successful launch
while [ ! -f /run/gcs_manager/pid ]
do
    sleep 0.5
done
gcs_manager_pid=`cat /run/gcs_manager/pid`

# Now change ownership of the socket so that apache can use it.
# See /lib/systemd/system/gcs_manager.socket
chmod 600 /var/run/gcs_manager/sock
if [ -f /usr/sbin/apache2 ]
then
    # Debian/Ubuntu variants.
    chown www-data:www-data /var/run/gcs_manager/sock
else
    # Redhat/CentOS/Fedora variants.
    chown apache:apache /var/run/gcs_manager/sock
fi

###
### Launch GCS Assistant
###
echo 'Launching GCS Assistant'

# This process syncs Globus configuration between nodes. It will send log
# messages to stdout which are usually caught by systemd and sent onto 
# syslog. Instead, you'll see these log messages with 'docker logs'.
sudo -u gcsweb -g gcsweb /opt/globus/bin/globus-connect-server assistant &
gcs_manager_assistant_pid=$!


###
### Launch Apache httpd
###
echo 'Launching Apache httpd'

if [ -f /usr/sbin/apache2 ]
then
    # Debian/Ubuntu variants. It is safe to use apachectl
    /usr/sbin/apachectl start
    pidfile=/var/run/apache2/apache2.pid
else
    # Redhat/CentOS/Fedora variants
    if [ ! -f /etc/pki/tls/certs/localhost.crt ]
    then
        # CentOS 8 would have called this through systemctl
        /usr/libexec/httpd-ssl-gencerts
    fi
    # apachectl uses systemctl
    /usr/sbin/httpd
    pidfile=/var/run/httpd/httpd.pid
fi

while [ ! -f $pidfile ]
do
    sleep 0.5
done
httpd_pid=`cat $pidfile`

###
### Launch GridFTP
###
echo 'Launching GridFTP Server'
sudo -u gcsweb -g gcsweb touch /var/lib/globus-connect-server/gcs-manager/gridftp-key
/usr/sbin/globus-gridftp-server \
    -S \
    -c /etc/gridftp.conf \
    -C /etc/gridftp.d \
    -log-module syslog \
    -d ERROR,WARN,INFO,TRANSFER \
    -bs 4194304 \
    -pidfile /run/globus-gridftp-server.pid

while [ ! -f /run/globus-gridftp-server.pid ]
do
    sleep 0.5
done
gridftp_pid=`cat /run/globus-gridftp-server.pid`


###############################################################################
# There is currently no support for OIDC without systemd.
###############################################################################

function is_process_alive()
{
    kill -0 $1 > /dev/null 2>&1
}

function check_process()
{
    # $1 - Name
    # $2 - PID

    is_process_alive $2
    if [ $? -ne 0 ]
    then
        echo "$1 exitted unexpectedly"
        return 1
    fi
    return 0
}

shutting_down=0
function cleanup()
{
    echo "Shutting down..."
    shutting_down=1
    trap - TERM
    trap - EXIT

    shutdown_gcs_manager=0
    is_process_alive $gcs_manager_pid && shutdown_gcs_manager=1
    if [ $shutdown_gcs_manager -eq 1 ]
    then
        echo "Terminating GCS Manager (pid $gcs_manager_pid)"
        kill -TERM $gcs_manager_pid

        first_pass=1
        while :
        do
            is_process_alive $gcs_manager_pid && break
	    (($first_pass)) && echo "Waiting on GCS Manager to exit"
	    first_pass=0
        done
        echo "GCS Manager has exitted"
    fi

    ###########################################################################
    # We do not wait for GridFTP transfers to complete.
    ###########################################################################

    #echo "Running node cleanup ..."
    #globus-connect-server node cleanup
    #we don't need to disable apache modules and whatever.
}

trap cleanup TERM
trap cleanup EXIT
trap '' HUP
trap '' INT

echo "GCS container successfully deployed"
while [ $shutting_down -eq 0 ]
do
    check_process "Apache httpd" $httpd_pid || shutting_down=1
    check_process "GCS Manager" $gcs_manager_pid || shutting_down=1
    check_process "GCS Manager Assistant" $gcs_manager_assistant_pid || shutting_down=1

    if [ $shutting_down -eq 1 ]
    then
        cleanup
    else
        sleep 1
    fi
done
