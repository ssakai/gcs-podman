#!/bin/bash

SECRETS_BASE_DIR=/var/secrets/gcs
MANAGER_ROOT_PERSIST=/var/secrets/gcs-manager-persist

# These will be mounted into the container's /export/ directory.
# When configuring a collection, set /export/ as the base_path, never 
# /.
COLLECTION_DIRS=( \
  "/var/globus" \
  )

umask 0077
set -u

# switch to a known location so
# relative paths work as expected.
cd $( dirname $0 )

if [[ "${#@}" -ne 1 ]]; then
  echo "Usage: $0 <ip-of-node|manager>" 1>&2
  exit 1
fi

if [[ ! -f "collection-dirs" ]]; then
  echo "Missing $(pwd)/collection-dirs file. Try collection-dirs.example as a starting point." 1>&2
  exit 1
fi

source "collection-dirs"
if [[ "${#COLLECTION_DIRS[@]}" -lt 1 ]]; then
  echo "No collection dirs specified in $(pwd)/collection-dirs, the GCS container would not be very useful." 1>&2
  exit 1
fi

# Node containers and the manager containers have some different create
# args. Add them to this array.
container_args=()

# Because --ip is broken on the version of podman shipped with
# CentOS 7, we can't specify an IP, except by using ipam to
# dole out a single IP.
#

# Non-manager nodes have some extra requirements on the container host.
if [[ "${1}" != "manager" ]]; then

  # A gcs-<ip> podman network.
  podman network inspect "gcs-${1}" > /dev/null
  if [[ $? -ne 0 ]]; then
    echo "Podman network gcs-${1} doesn't exist. Please create it so that a single IP, corresponding to this script's arguments, is assigned to the container." 1>&2
    echo "See podman-network.conflist.example" 1>&2
    exit 1
  fi

  # A <ip>.nodecfg.json file, in the secrets directory.
  if [[ ! -f "${SECRETS_BASE_DIR}/${1}.nodecfg.json" ]]; then
    echo "Missing node config file: ${SECRETS_BASE_DIR}/${1}.nodecfg.json"  1>&2
    echo "Try using manager container to 'globus-connect-server node create ... --export-node nodecfg.json'" 1>&2
    exit 1
  fi
 
  # All good? Add the args.

  # Secrets
  container_args+=( "--mount=type=bind,src=${SECRETS_BASE_DIR},destination=/secrets,ro,rslave" ) 

  # Network
  container_args+=( "--net=gcs-${1}" )

  # Exports
  for C in "${COLLECTION_DIRS[@]}"; do
    container_args+=("--mount=type=bind,src=${C},destination=/export/${C},bind-propagation=rslave")
  done

  # Entrypoint
  container_args+=( --entrypoint='["/usr/bin/tini", "/entrypoint.sh"]' )

  # /root is tmpfs since globus-connect-server writes stuff there.
  container_args+=( --mount=type=tmpfs,tmpfs-size=100M,destination=/root )

else
  # This is the manager
  # 
  # A manager container can use the standard podman nat bridge. It doesn't matter
  # what the manager's IP is, as long as it can reach the globus servers and our gcs
  # containers.

  # Root's homedir needs to be persistent as globus-connect-server writes tokens to
  # it, used for authenticating for endpoint/node/collection management.
  if [[ ! -d "${MANAGER_ROOT_PERSIST}" ]]; then
    echo "Missing manager container persistent storage directory: ${MANAGER_ROOT_PERSIST}"
    exit 1
  fi
  statcode=$(stat -c %a "${MANAGER_ROOT_PERSIST}")
  if [[ "${statcode}" != "700" ]]; then
    echo "Bad permissions on manager container persistent storage directory: ${MANAGER_ROOT_PERSIST} expected: 700 got: ${statcode}"
    exit 1
  fi
  container_args+=( --mount=type=bind,src="${MANAGER_ROOT_PERSIST}",destination=/root,rw )

  # 
  # Add in args

  # Secrets  (read/write because we create node cfg in this container)
  container_args+=( "--mount=type=bind,src=${SECRETS_BASE_DIR},destination=/secrets,rw" ) 

  # network is the whatever default.
  # skipping export dirs since the manager doesn't serve.

  # use a placeholder entrypoint because the default one will start a server.
  container_args+=( --entrypoint='["/bin/sleep", "1200"]' )
  
fi


# Older versions of podman don't have a --replace function, so let's just kill the existing container.
podman rm -f "gcs-${1}"

# Make the new one
podman run \
  --rm -i -t \
  --user=0 \
  --read-only \
  --dns=1.1.1.1 \
  --cap-drop=all \
  --cap-add=KILL \
  --cap-add=NET_BIND_SERVICE \
  --cap-add=SETGID \
  --cap-add=SETUID \
  --cap-add=CHOWN \
  --cap-add=FOWNER \
  --cap-add=DAC_OVERRIDE \
  --security-opt=seccomp=unconfined \
  --security-opt=no-new-privileges \
  --mount=type=tmpfs,tmpfs-size=100M,destination=/var/run \
  --mount=type=tmpfs,tmpfs-size=1024M,destination=/var/log \
  --mount=type=tmpfs,tmpfs-size=200M,destination=/var/lib/globus-connect-server \
  --mount=type=tmpfs,tmpfs-size=200M,destination=/etc/grid-security \
  --mount=type=tmpfs,tmpfs-size=20M,destination=/etc/gridftp.d \
  --mount=type=bind,src=/dev/log,destination=/dev/log,rw \
  --mount=type=bind,src=/var/run/nscd/socket,destination=/var/run/nscd/socket,rw \
  --name=gcs-${1} \
  --hostname=gcs-${1} \
  "${container_args[@]}" \
  --entrypoint="/bin/bash" \
  globus-connect-server54:latest 

if [[ $? -ne 0 ]]; then
  echo "Failed to create container." 1>&2
  exit 1
fi

printf "New container %s\nStart with:\n  podman start %s\n" "gcs-${1}" "gcs-${1}"
