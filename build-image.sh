#!/bin/bash

cd $(dirname $0)

TEMPFILE=$(mktemp)
if [[ ! -f "${TEMPFILE}" ]]; then
  echo "Failed to create tempfile with mktemp" 1>&2
  exit 1
fi

# Build the image and save the output for later.
# It contains the image id.
podman build  --no-cache -t globus-connect-server54:latest \
  -f Dockerfile | tee "${TEMPFILE}"

if [[ $? -ne 0 ]]; then
  echo "Failed to create image. Return code was $?" 1>&2
  rm -f "${TEMPFILE}"
  exit 1
fi

IMGID=$( tail -1 "${TEMPFILE}" )

# Get GCS version
GCSVER=$( podman run --rm --net=none --entrypoint="globus-connect-server" "${IMGID}" --version | awk -F, '{print $2}' | awk '{print $2}' )

# Get Deb version
DEBVER=$( podman run --rm --net=none --entrypoint="/bin/cat" "${IMGID}" /etc/debian_version )

IMGTAG=$( printf "gcs-%s-deb-%s" "${GCSVER}" "${DEBVER}" )
podman tag "${IMGID}" "globus-connect-server:${IMGTAG}"
echo "New image: globus-connect-server:${IMGTAG}"

rm -f "${TEMPFILE}"
