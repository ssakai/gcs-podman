FROM debian:11-slim

# Original copied from https://github.com/globus/globus-connect-server-deploy/tree/master/distros


LABEL maintainer="ssakai@sdsc.edu"

ARG USE_UNSTABLE_REPOS
ARG USE_TESTING_REPOS

ARG REPO_PREFIX=https://downloads.globus.org/globus-connect-server
ARG REPO_SUFFIX=installers/repo/deb/globus-repo_latest_all.deb

ENV LC_ALL C.UTF-8
ENV DEBIAN_FRONTEND="noninteractive"

RUN \
    apt-get update                                                                                       ;\
    apt-get install -y curl gnupg dialog apt-utils sudo psmisc iperf net-tools tcpdump \
            tzdata netcat strace tini iputils-ping traceroute ;\
    if [ -n "$USE_UNSTABLE_REPOS" ]; then                                                                 \
        echo "Using unstable package repositories!"                                                      ;\
        curl -LOs $REPO_PREFIX/unstable/$REPO_SUFFIX                                                     ;\
        dpkg -i globus-repo_latest_all.deb                                                               ;\
        sed -i /etc/apt/sources.list.d/globus-connect-server-unstable*.list -e 's/^# deb /deb /'         ;\
        sed -i /etc/apt/sources.list.d/globus-connect-server-stable*.list -e 's/^deb /# deb /'           ;\
    elif [ -n "$USE_TESTING_REPOS" ]; then                                                                \
        echo "Using testing package repositories!"                                                       ;\
        curl -LOs $REPO_PREFIX/testing/$REPO_SUFFIX                                                      ;\
        dpkg -i globus-repo_latest_all.deb                                                               ;\
        sed -i /etc/apt/sources.list.d/globus-connect-server-testing*.list -e 's/^# deb /deb /'          ;\
        sed -i /etc/apt/sources.list.d/globus-connect-server-stable*.list -e 's/^deb /# deb /'           ;\
    else                                                                                                  \
        echo "Using stable package repositories!"                                                        ;\
        curl -LOs $REPO_PREFIX/stable/$REPO_SUFFIX                                                       ;\
        dpkg -i globus-repo_latest_all.deb                                                               ;\
    fi                                                                                                   ;\
    APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=1 apt-key add /usr/share/globus-repo/RPM-GPG-KEY-Globus         ;\
    apt-get update                                                                                       ;\
    apt-get install -y globus-connect-server54                                                           ;\
    echo "Removing setuid/setgid bits"                                                                   ;\
    rm -r /bin/systemctl                                                                                 ;\
    ln -s /bin/true /bin/systemctl                                                                       ;\
    a2enmod headers                                                                                      ;\
    a2enmod proxy                                                                                        ;\
    a2enmod proxy_http                                                                                   ;\
    a2enmod rewrite                                                                                      ;\
    a2enmod setenvif                                                                                     ;\
    a2enmod mime                                                                                         ;\
    a2enmod socache_shmcb                                                                                ;\
    a2enmod ssl                                                                                          ;\
    a2enconf tls-mod-globus                                                                               ;\
    ln -s /bin/true /bin/csh                                                                             ;\
    ln -s /bin/true /bin/ksh                                                                             ;\
    ln -s /bin/true /bin/tcsh                                                                            ;\
    ln -s /bin/true /bin/zsh                                                                             ;\
    mv /var/lib/globus-connect-server /var/lib/globus-connect-server-dist/                               ;\
    mv /etc/grid-security /etc/grid-security-dist/                                                       ;\
    truncate -s 0 /etc/passwd                                                                    ;\
    truncate -s 0 /etc/group                                                                     ;\
    find / -path /proc -prune -o \( -perm /2000 -o -perm /4000 \) -a -type f -ls -exec chmod g-s,u-s '{}' \;
    

COPY entrypoint.sh /entrypoint.sh
COPY mapapp.py /mapapp.py
COPY restrict-paths.json /default-restrict-paths.json
COPY shadow.empty /etc/shadow
RUN  chown 0:0 /etc/shadow; chmod 600 /etc/shadow

# Default command unless overriden with 'docker run --entrypoint'
ENTRYPOINT ["/bin/tini", "/entrypoint.sh"]
# Default options to ENTRYPOINT unless overriden with 'docker run arg1...'
CMD []
